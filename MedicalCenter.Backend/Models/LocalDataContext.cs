﻿
namespace MedicalCenter.Backend.Models
{
    using Domain;

    public class LocalDataContext : DataContext
    {
        public System.Data.Entity.DbSet<MedicalCenter.Domain.User> Users { get; set; }

        public System.Data.Entity.DbSet<MedicalCenter.Domain.UserType> UserTypes { get; set; }
        //  public System.Data.Entity.DbSet<MedicalCenter.Domain.User> Users { get; set; }

        //   public System.Data.Entity.DbSet<MedicalCenter.Domain.UserType> UserTypes { get; set; }
    }
}