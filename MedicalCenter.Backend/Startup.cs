﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MedicalCenter.Backend.Startup))]
namespace MedicalCenter.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
