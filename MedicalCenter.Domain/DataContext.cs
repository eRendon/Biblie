﻿

namespace MedicalCenter.Domain
{
    using System.Data.Entity;

    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {

        }

        public System.Data.Entity.DbSet<MedicalCenter.Domain.User> Users { get; set; }

        public System.Data.Entity.DbSet<MedicalCenter.Domain.UserType> UserTypes { get; set; }

        //public System.Data.Entity.DbSet<MedicalCenter.Domain.User> Users { get; set; }
    }
}
