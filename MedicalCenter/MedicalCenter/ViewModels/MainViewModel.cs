﻿

namespace MedicalCenter.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Domain;
    using Models;
    using ViewModels;
    using Helpers;

    public class MainViewModel : BaseViewModel
    {
        #region Atributes
        private UserLocal user;
        #endregion

        #region ViewModels
        public LoginViewModel Login
        {
            get;
            set;
        }


        public HomeViewModel Home
        {
            get;
            set;
        }



        public RegisterViewModel  Register
        {
            get; 
            set;
        }

        public  ChangePasswordViewModel ChangePassword
        {
            get;
            set;
        }
        #endregion

        #region Properties
        public string SelectedModule
        {
            get;
            set;
        }

        public TokenResponse Token
        {
            get;
            set;
        }


        public ObservableCollection<MenuItemViewModel> Menus
        {
            get;
            set;
        }

        public UserLocal User
        {
            get { return this.user;  }
            set { SetValue(ref this.user, value);  }
        }

        public MyProfileViewModel MyProfile
        {
            get;
            set;
        }

        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;

            this.Login = new LoginViewModel();
            this.LoadMenu();
        }
        #endregion

        #region Singleton
        private static MainViewModel instance;

        

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion

        #region Methods
        private void LoadMenu()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>();

            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_settings_app",
                PageName = "MyProfilePage",
                Title = Languages.MyProfile
            });
                      
            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_exit_to_app",
                PageName = "LoginPage",
                Title = Languages.LogOut
            });

        }
        #endregion

    }
}