﻿

namespace MedicalCenter.ViewModels
{
    using MedicalCenter.Helpers;
    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using Xamarin.Forms;
    using MedicalCenter.Views;

    public class MenuItemViewModel
    {
        public string Icon { get; set; }

        public string Title { get; set; }

        public string PageName { get; set; }

        #region Commands
        public ICommand NavigationCommand
        {
            get
            {
                return new RelayCommand(Navigate);
            }            
        }

        private void Navigate()
        {
            App.Master.IsPresented = false;

            if (this.PageName == "LoginPage")
            {
                Settings.IsRemembered = "false";                
                var mainViewmodel = MainViewModel.GetInstance();
                mainViewmodel.Token = null;
                mainViewmodel.User = null;
                Application.Current.MainPage = new NavigationPage(new LoginPage());
            }
            else if (this.PageName == "MyProfilePage")
            {
                MainViewModel.GetInstance().MyProfile = new MyProfileViewModel();                
                App.Navigator.PushAsync(new MyProfilePage());

            }
        }
        #endregion
    }
}
